@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">List of Users</div>

                <div class="panel-body">
                    @if (count($userlist) >= 1)
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th colspan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($userlist as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        No records to list!
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
