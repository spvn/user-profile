<!doctype HTML>
<html>
    <head>
        <title>{{ $profile->first_name }} | Profiles</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/paper/bootstrap.min.css" rel="stylesheet" integrity="sha384-2mX2PSpkRSXLQzmNzH3gwK6srb06+OfbDlYjbog8LQuALYJjuQ3+Yzy2JIWNV9rW" crossorigin="anonymous">

        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
                    <div class="well profile">
                        <div class="col-sm-12">
                            <div class="col-xs-12 col-sm-8">
                                <h2>{{ $profile->first_name }} {{ $profile->last_name }}</h2>
                                <p><strong>About: </strong> {{ $profile->user_bio }} </p>
                                <p><strong>Email: </strong> {{ $profile->email }} </p>
                                <p><strong>Skills: </strong>
                                    <span class="tags">html5</span> 
                                    <span class="tags">css3</span>
                                    <span class="tags">jquery</span>
                                    <span class="tags">bootstrap3</span>
                                </p>
                            </div>             
                            <div class="col-xs-12 col-sm-4 text-center">
                                <figure>
                                    <img src="http://www.localcrimenews.com/wp-content/uploads/2013/07/default-user-icon-profile.png" alt="" class="img-circle img-responsive">
                                    <figcaption class="ratings">
                                        <p>Ratings
                                            <a href="#">
                                                <span class="fa fa-star"></span>
                                            </a>
                                            <a href="#">
                                                <span class="fa fa-star"></span>
                                            </a>
                                            <a href="#">
                                                <span class="fa fa-star"></span>
                                            </a>
                                            <a href="#">
                                                <span class="fa fa-star"></span>
                                            </a>
                                            <a href="#">
                                                <span class="fa fa-star-o"></span>
                                            </a> 
                                        </p>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>            
                        <div class="col-xs-12 divider text-center">
                            <div class="col-xs-12 col-sm-4 emphasis">
                                <h2><strong> 20,7K </strong></h2>                    
                                <p><small>Followers</small></p>
                                <button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span> Follow </button>
                            </div>
                            <div class="col-xs-12 col-sm-4 emphasis">
                                <h2><strong>245</strong></h2>                    
                                <p><small>Following</small></p>
                                <button class="btn btn-info btn-block"><span class="fa fa-user"></span> View Profile </button>
                            </div>
                            <div class="col-xs-12 col-sm-4 emphasis">
                                <h2><strong>43</strong></h2>                    
                                <p><small>Snippets</small></p>
                                <div class="btn-group dropup btn-block">
                                    <button type="button" class="btn btn-primary"><span class="fa fa-gear"></span> Options </button>
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu text-left" role="menu">
                                        <li><a href="#"><span class="fa fa-envelope pull-right"></span> Send an email </a></li>
                                        <li><a href="#"><span class="fa fa-list pull-right"></span> Add or remove from a list  </a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><span class="fa fa-warning pull-right"></span>Report this user for spam</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" class="btn disabled" role="button"> Unfollow </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>                 
                </div>
            </div>
        </div>
    </body>
</html>