# User Profile Manager

User Profile Manager is developed using Laravel 5.2 Framework. It can be used managing and sharing profiles of users within the establishment. Listed below are some of the places where User Profile Manager can be used.

> Corporate Employee Profiles with designation, skills, experience
> Student Profile with Attendance and Test Performance
> Doctor Profile with specialization and appointment availability

## License

User Profile Manager is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
