<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Profile;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Below line is commented to add authentication to route group
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function userList()
    {
        $profiles=new Profile;
        $data['userlist']=$profiles->all();
        return view('userlist',$data);
    }
    
    public function showProfile($id)
    {
        $profile=new Profile;
        $data['profile']=$profile->where('id',$id)->first();
        //dd($data);
        return view('showprofile',$data);
    }
}
