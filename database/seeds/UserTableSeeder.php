<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'Administrator',
            'email'=>'admin@userprofile.app',
            'password'=>  bcrypt('admin@123')
        ]);
    }
}
